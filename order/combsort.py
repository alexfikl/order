from order.insertionsort import sort as insertionsort


def sort(array, key=lambda x: x, reverse=False):
    '''Comb sort improves on bubble sort, and rivals algorithms like Quicksort.
    The basic idea is to eliminate turtles, or small values near the end of the
    list, since in a bubble sort these slow the sorting down tremendously.
    Rabbits, large values around the beginning of the list, do not pose a
    problem in bubble sort.

    With this idea in mind we compare elements with smaller and smaller gaps
    between them (eventually doing a normal bubble sort, i.e. gap of 1) so that
    the small elements at the end can get tot he beginning faster.

    When the gap becomes small, we pass to Insertion sort which is quite good
    on partially sorted arrays.

    Running time: worst case O(n ** 2), but an average case of O(n logn).
    '''
    if len(array) <= 1:
        return array

    # see: http://en.wikipedia.org/wiki/Comb_sort#Shrink_factor
    n = len(array)
    gap = int(n / 1.24733095)

    swapped = True
    while gap > 2 or swapped:   # at gap 2 we do an Insertion sort
        swapped = False
        for i in range(n - gap):
            if key(array[i]) > key(array[i + gap]):
                array[i], array[i + gap] = array[i + gap], array[i]
                swapped = True

        gap = int(gap / 1.24733095)

    array = insertionsort(array, key)
    return array[::-1] if reverse else array
