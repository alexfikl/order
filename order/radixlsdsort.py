import math


def _split(array, base, power):
    buckets = [[] for x in range(base)]
    for value in array:
        digit = (value // base ** power) % base
        buckets[digit].append(value)

    return buckets


def sort(array, key=lambda x: x, reverse=False):
    '''Least Significant Digit version.
    Algorithm:
    - take the least significant digit of each key
    - group keys based on that digit
    - repeat grouping process with the next digit
    - sorting of groups is done with bucket sort
    Running time: O(k * n) where n is the input size and k is the average
    length of a key.
    '''
    try:
        base = 10
        power = int(math.log(max(array), base) + 1)
    except TypeError:
        raise ValueError("Radix sort only supports integers.") from None

    for k in range(power):
        array = _split(array, base, k)
        array = [i for row in array for i in row]

    return array[::-1] if reverse else array
