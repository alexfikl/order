def sort(array, key=lambda x: x, reverse=False):
    '''Cocktail sort is a variation of bubble sort that is both a stable
    sorting algorithm and a comparison sort. The algorithm differs from a
    bubble sort in that it sorts in both directions on each pass through
    the list.

    Running time: O(n ** 2), becomes closer to O(n) if partially sorted.
    '''
    if len(array) <= 1:
        return array

    begin = 0
    end = len(array) - 1

    swapped = True
    while swapped:
        swapped = False
        for i, j in zip(range(begin, end), range(end, begin, -1)):
            # from the beginning
            if key(array[i]) > key(array[i + 1]):
                array[i], array[i + 1] = array[i + 1], array[i]
                swapped = True

            # from the end
            if key(array[j]) < key(array[j - 1]):
                array[j], array[j - 1] = array[j - 1], array[j]
                swapped = True

        begin += 1
        end -= 1

    return array[::-1] if reverse else array
