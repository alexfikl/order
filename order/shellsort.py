def _jumps(n):
    inc = n // 2
    while inc > 0:
        yield inc
        inc = inc // 2


def sort(array, key=lambda x: x, reverse=False):
    '''Shellsort is a generalisation of exchange-based sorting algorithms like
    Insertion Sort and Bubble Sort. It is based on the fact that Insertion Sort
    works rather well for almost sorted arrays, so it tries to bring the array
    to such a state.

    The algorithm first compares and swaps elements that are further away
    (starting from n / 2, then n / 4, etc) and finally finishes with a step of
    1 which is equivalent to Insertion Sort.
    .
    Running time: O(n ** 3/2).
    '''
    for h in _jumps(len(array)):
        for i in range(h, len(array)):
            current = array[i]
            j = i
            while j >= h and key(array[j - h]) > key(current):
                array[j] = array[j - h]
                j -= h
            array[j] = current

    return array[::-1] if reverse else array
