def sort(array):
    '''wee.
    Algorithm:
    - pulls a sorted sublist from the array
    - merges the sublist with the result array
    - ta da!
    Running time: O(n ** 2)
    Works really well on partially sorted array.
    '''
    result = []
    while len(array) > 0:
        sublist = []
        sublist.append(array.pop(0))
        last = len(sublist) - 1
        i = 0
        while i < len(array):
            if array[i] > sublist[last]:
                sublist.append(array.pop(i))
                last += 1
            i += 1

        if result:
            spliced = False
            for val in sublist:
                for i, rval in enumerate(result):
                    if val < rval:
                        result.insert(i, val)
                        spliced = True
                        break

                if not spliced:
                    result.append(val)
        else:
            result += sublist

    return result
