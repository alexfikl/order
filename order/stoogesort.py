def _recsort(array, i, j, key):
    if key(array[i]) > key(array[j]):
        array[i], array[j] = array[j], array[i]
    if (j - i) > 1:
        t = (j - i + 1) // 3
        _recsort(array, i, j - t, key)
        _recsort(array, i + t, j, key)
        _recsort(array, i, j - t, key)


def sort(array, key=lambda x: x, reverse=False):
    '''Stooge Sort is a recursive sorting algorithm that manages to be slower
    than Bubble Sort.

    The algorithm works as follows; if the value at the end is smaller than the
    value at the beginning then it swaps them, else, if the array has 3 or more
    elements then it:
    * sorts the first 2/3 of the array,
    * sorts the last 2/3 of the array and
    * sorts the first 2/3 again

    Running time: O(n^2.7095).
    '''
    _recsort(array, 0, len(array) - 1, key)
    return array[::-1] if reverse else array
