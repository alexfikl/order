# see:
#   http://www.iti.fh-flensburg.de/lang/algorithmen/sortieren/bitonic/oddn.htm


def _merge(array, key, direction):
    if len(array) <= 1:
        return array

    #middle = 2 ** (int(log2(len(array))) - 1)
    middle = len(array) // 2
    array = _compare(array, middle, key, direction)

    first = _merge(array[:middle], key, direction)
    second = _merge(array[middle:], key, direction)

    return first + second


def _compare(array, middle, key, direction):
    for i in range(middle):
        if direction == (key(array[i]) > key(array[i + middle])):
            array[i], array[i + middle] = array[i + middle], array[i]

    return array


def _recsort(array, key, direction):
    if len(array) <= 1:
        return array

    #middle = 2 ** (int(log2(len(array))) - 1)
    middle = len(array) // 2
    first = _recsort(array[:middle], key, direction)
    second = _recsort(array[middle:], key, not direction)

    return _merge(first + second, key, direction)


def sort(array, key=lambda x: x, reverse=False):
    '''Bitonic Mergesort is a parallel sorting algorithm.

    The name comes from the idea of a `bitonic sequence`: a monotonic sequence
    is a sorted sequence (increasing or decreasing), a bitonic sequence is a
    sequence where:
    x_0 <= x_1 <= ... <= x_k >= x_{k + 1} ... >= x_n

    The algorithm is very similar to the classic Mergesort, as in: it splits
    the array in half, sorts one half increasingly and the second half
    decreasingly and then merges them.

    Running time: O(n log^2(n)).
    '''
    if len(array) <= 1:
        return array

    array = _recsort(array, key, not reverse)
    return array
