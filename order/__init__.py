import sys

__title__ = 'order'
__version__ = '0.1'
__build__ = 0x0001
__author__ = 'Alexandru Fikl'
__license__ = 'MIT License'
__copyright__ = 'Copyright (c) 2013 Alexandru Fikl'

__all__ = [
    "bitonicsort",
    "bogosort",
    "bubblesort",
    "bucketsort",
    "cocktailsort",
    "combsort",
    "countingsort",
    "cyclesort",
    "gnomesort",
    "heapsort",
    "insertionsort",
    "introsort",
    "librarysort",
    "mergesort",
    "oddevensort",
    "patiencesort",
    "pigeonholesort",
    "quicksort",
    "radixlsdsort",
    "selectionsort",
    "shellsort",
    "smoothsort",
    "spreadsort",
    "stoogesort",
    "strandsort",
    "timsort",
    "tournamentsort",
    "treesort"
]

# little shortcut so that we can call the function in the module nicely
for module in __all__:
    fullmodule = "order.{}".format(module)
    fullmodule = __import__(fullmodule, fromlist=['sort'])
    setattr(sys.modules[__name__], module, fullmodule.sort)

del fullmodule
del module
