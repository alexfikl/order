def sort(array, key=lambda x: x, reverse=False):
    '''Cycle sort is an in-place, unstable sorting algorithm, a comparison sort
    that is theoretically optimal in terms of the total number of writes to the
    original array, unlike any other in-place sorting algorithm.

    Running time: O(n^2).
    '''
    if len(array) <= 1:
        return array

    # see: http://en.wikipedia.org/wiki/Cycle_sort
    for start in range(len(array) - 1):
        item = array[start]
        pos = start

        # find out where to put the item,
        # i.e, how many elements are smaller than it
        pos += sum([key(x) < key(item) for x in array[start + 1:]])

        # we're at the good place
        if pos == start:
            continue

        # put the item after any duplicates
        while key(item) == key(array[pos]):
            pos += 1
        array[pos], item = item, array[pos]

        # rotate the rest of the cycle
        while pos != start:
            pos = start + sum([key(x) < key(item) for x in array[start + 1:]])

            while key(item) == key(array[pos]):
                pos += 1
            array[pos], item = item, array[pos]

    return array[::-1] if reverse else array
