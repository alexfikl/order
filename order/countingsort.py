from order.pigeonholesort import sort as pidgeonholesort


def sort(array, key=lambda x: x, reverse=False):
    '''Counting sort is an algorithm for sorting a collection of objects
    according to keys that are small integers; that is, it is an integer
    sorting algorithm. It operates by counting the number of objects that
    have each distinct key value, and using arithmetic on those counts to
    determine the positions of each key value in the output sequence.

    Running time: O(n + k) where n is the input size and k the counting
    array size.
    '''
    return pidgeonholesort(array, key, reverse)
