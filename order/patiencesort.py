def _lt_piles(x, y, key=lambda x: x):
    if x == y:
        return False

    # we also need to know which one has the biggest elements
    for xi, yi in zip(x, y):
        if key(xi) == key(yi):
            continue

        return False if key(xi) < key(yi) else True

    return False if len(x) < len(y) else True


def _bisect_left_with_key(a, x, lo=0, hi=None, key=lambda x: x, lt=_lt_piles):
    if hi is None:
        hi = len(a)

    while lo < hi:
        middle = (lo + hi) // 2
        if lt(x, a[middle], key):
            lo = middle + 1
        else:
            hi = middle

    return lo


def sort(array, key=lambda x: x, reverse=False):
    '''Patience sorting is a sorting algorithm, based on a solitaire card game.

    The algorithm is rather simple. It involves building up sorted piles, like
    in the game and then selecting elements from the piles so as to always
    pick the biggest/smallest element.

    Running time: O(n lgn).
    '''
    piles = []
    for element in array:
        i = _bisect_left_with_key(piles, [element], key=key)
        if i != len(piles):
            piles[i].insert(0, element)
        else:
            piles.append([element])

    for i in range(len(array)):
        small_pile = piles[0]
        array[i] = small_pile.pop(0)
        if not small_pile:
            piles.pop(0)
        else:
            piles.pop(0)
            i = _bisect_left_with_key(piles, small_pile, key=key)
            piles.insert(i, small_pile)

    return array[::-1] if reverse else array
