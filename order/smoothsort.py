def sort(array):
    '''Similar to heapsort.
    LP  - http://en.wikipedia.org/wiki/Leonardo_number
        - used for the up/down functions in smoothsort
    MultiplyDeBruijnBitPosition
        - http://www.0xe3.com/text/ntz/ComputingTrailingZerosHOWTO.html
        - gets the number of trailing zeroes from a binary representation
    Algorithm:
    TODO
    Running time: O(n *lgn)
    Pretty awesome. Developed by Dijsktra.
    '''
    LP = [
        1, 1, 3, 5, 9, 15, 25, 41, 67, 109, 177, 287, 465, 753, 1219, 1973,
        3193, 5167, 8361, 13529, 21891, 35421, 57313, 92735, 150049, 242785,
        392835, 635621, 1028457, 1664079, 2692537, 4356617, 7049155, 11405773,
        18454929, 29860703, 48315633, 78176337, 126491971, 204668309,
        331160281, 535828591, 866988873
    ]

    MultiplyDeBruijnBitPosition = [
        0,  1, 28,  2, 29, 14, 24, 3, 30, 22, 20, 15, 25, 17,  4, 8, 31, 27,
        13, 23, 21, 19, 16, 7, 26, 12, 18,  6, 11,  5, 10, 9
    ]

    def trailingzeroes(v):
        '''gotta love magic numbers.'''
        position = (((v & -v) * 0x077CB531) >> 27) & 0b11111
        return MultiplyDeBruijnBitPosition[position]

    def sift(array, pshift, head):
        while pshift > 1:
            rt = head - 1
            lf = head - 1 - LP[pshift - 2]
            if array[head] >= array[lf] and array[head] >= array[rt]:
                break
            if array[lf] >= array[rt]:
                array[head], array[lf] = array[lf], array[head]
                head = lf
                pshift -= 1
            else:
                array[head], array[rt] = array[rt], array[head]
                head = rt
                pshift -= 2

    def trinkle(array, p, pshift, head, trusty):
        while p != 1:
            stepson = head - LP[pshift]
            if array[stepson] <= array[head]:
                break
            if not trusty and pshift > 1:
                rt = head - 1
                lf = head - 1 - LP[pshift - 2]
                if array[rt] >= array[stepson] or array[lf] >= array[stepson]:
                    break
            array[head], array[stepson] = array[stepson], array[head]
            head = stepson
            trail = trailingzeroes(p & ~1)
            p >>= trail
            pshift += trail
            trusty = False

        if not trusty:
            sift(array, pshift, head)

    p = 1
    pshift = 1
    head = 0
    while head < len(array) - 1:
        if (p & 3) == 3:
            sift(array, pshift, head)
            p >>= 2
            pshift += 2
        else:
            if LP[pshift - 1] >= len(array) - 1 - head:
                trinkle(array, p, pshift, head, False)
            else:
                sift(array, pshift, head)

            if pshift == 1:
                p <<= 1
                pshift -= 1
            else:
                p <<= pshift - 1
                pshift = 1

        p |= 1
        head += 1
    trinkle(array, p, pshift, head, False)

    while pshift != 1 or p != 1:
        if pshift <= 1:
            trail = trailingzeroes(p & ~1)
            p >>= trail
            pshift += trail
        else:
            p <<= 2
            p ^= 7
            pshift -= 2

            trinkle(array, p >> 1, pshift + 1, head - LP[pshift] - 1, True)
            trinkle(array, p, pshift, head - 1, True)
        head -= 1

    return array
