def _tree_insert(tree, element, key):
    if key(tree[0]) > key(element):
        if not tree[1]:
            tree[1] = [element, [], []]
        else:
            tree[1] = _tree_insert(tree[1], element, key)
    else:
        if not tree[2]:
            tree[2] = [element, [], []]
        else:
            tree[2] = _tree_insert(tree[2], element, key)

    return tree


def _tree_build(array, key):
    tree = [array[0], [], []]
    for element in array[1:]:
        tree = _tree_insert(tree, element, key)

    return tree


def _tree_inorder(tree):
    # NOTE: returns work in a generator?
    if not tree:
        return

    for el in _tree_inorder(tree[1]):
        yield el

    yield tree[0]

    for el in _tree_inorder(tree[2]):
        yield el


def sort(array, key=lambda x: x, reverse=False):
    '''Tree sort is a sort algorithm that builds a binary search tree from
    the keys to be sorted, and then traverses the tree (in-order) so that the
    keys come out in sorted order. The algorithm can have O(n logn) running
    time if the binary tree is balanced.

    Running time: O(n^2).
    '''
    array = _tree_build(array, key)
    array = list(_tree_inorder(array))

    return array[::-1] if reverse else array
