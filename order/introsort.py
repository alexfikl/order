from math import log2
import random

from order.heapsort import sort as heapsort


def _depthsort(array, depth, key):
    if len(array) <= 1:
        return array

    if depth == 0:
        return heapsort(array, key)

    pivot = random.randrange(len(array))
    e = array.pop(pivot)

    left = _depthsort([x for x in array if key(x) < key(e)], depth - 1, key)
    right = _depthsort([x for x in array if key(x) >= key(e)], depth - 1, key)

    return left + [e] + right


def sort(array, key=lambda x: x, reverse=False):
    '''Introsort is a sorting algorithm developed by David Musser in 1997.
    It uses Quicksort and Heapsort when convenient to get over Quicksort's
    worst-case scenario of O(n^2).

    It usually switches to Heapsort when the heap would be a nice logn.

    Running time: O(n logn).
    '''
    if len(array) <= 1:
        return array

    array = _depthsort(array, int(log2(len(array))), key)
    return array[::-1] if reverse else array
