def _siftdown(array, start, end, key):
    root = start
    while root * 2 + 1 <= end:
        child = root * 2 + 1

        if child + 1 <= end and key(array[child]) < key(array[child + 1]):
            child += 1

        if child <= end and key(array[root]) < key(array[child]):
            array[root], array[child] = array[child], array[root]
            root = child
        else:
            return


def _heapify(array, key):
    n = len(array) - 1
    start = (n - 1) // 2
    while start >= 0:
        _siftdown(array, start, n, key)
        start -= 1

    return array


def sort(array, key=lambda x: x, reverse=False):
    '''Heapsort is a comparison-based sorting algorithm to create a sorted
    array (or list), and is part of the selection sort family. Although
    somewhat slower in practice on most machines than a well-implemented
    Quicksort, it has the advantage of a more favorable worst-case
    O(n log n) runtime. Heapsort is an in-place algorithm, but it is not
    a stable sort.

    The algorithm is very simple: first, we have to construct a heap from our
    array and then repeatedly remove the biggest element of the heap (i.e.
    the root) until the heap is empty.

    Running time: O(n * lgn).
    '''
    if len(array) <= 1:
        return array

    array = _heapify(array, key)
    end = len(array) - 1
    while end > 0:
        array[end], array[0] = array[0], array[end]
        _siftdown(array, 0, end - 1, key)
        end -= 1

    return array[::-1] if reverse else array
