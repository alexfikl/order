from order.insertionsort import sort as insertionsort


def _bucket_index(value, a, b, n):
    delta = (b - a) / n
    return min(int((value - a) / delta), n - 1)


def sort(array, key=lambda x: x, reverse=False):
    '''Bucket sort, or bin sort, is a sorting algorithm that works by
    partitioning an array into a number of buckets. Each bucket is then sorted
    individually by using Insertion sort and added to the sorted array.

    Running time: O(n + k), where k is the number of buckets, usually smaller
    than n.
    '''
    if len(array) <= 1:
        return array

    a = key(min(array, key=key))
    b = key(max(array, key=key))

    # this should give about 10 elements per bucket
    # for a uniform random variable, so maybe around
    # 20-ish for something not as uniform, which is
    # good for insertion sort
    n = len(array) // 10

    buckets = [[] for i in range(n)]
    for value in array:
        buckets[_bucket_index(key(value), a, b, n)].append(value)

    array = []
    for bucket in buckets:
        array.extend(insertionsort(bucket, key))

    return array[::-1] if reverse else array
