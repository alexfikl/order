def sort(array, key=lambda x: x, reverse=False):
    '''Gnome sort is a sorting algorithm which is similar to insertion sort,
    except that moving an element to its proper place is accomplished by a
    series of swaps, as in bubble sort.

    The algorithm always finds the first place where two adjacent elements
    are in the wrong order, and swaps them. It takes advantage of the fact
    that performing a swap can introduce a new out-of-order adjacent pair
    only right before or after the two swapped elements. It does not assume
    that elements forward of the current position are sorted, so it only
    needs to check the position directly before the swapped elements.

    Running time: O(n ** 2).
    '''
    if len(array) <= 1:
        return array

    i = 1
    while i < len(array):
        if key(array[i]) >= key(array[i - 1]):
            i += 1
        else:
            array[i], array[i - 1] = array[i - 1], array[i]
            i += -1 if i > 1 else 0

    return array[::-1] if reverse else array
