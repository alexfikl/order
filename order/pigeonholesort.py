def sort(array, key=lambda x: x, reverse=False):
    '''Pigeonhole sorting is a sorting algorithm that is suitable for sorting
    lists of elements where the number of elements and the number of possible
    key values are approximately the same.

    The algorithms goes as follows: first, set up an auxiliary array of empty
    `pigeonholes` and then put each element of the array in a specific hole
    and then just pull out the elements in order to get the sorted array.

    Running time: O(n + N) where n is the input size and N the number of key
    values.
    '''
    if len(array) <= 1:
        return array

    left = key(min(array, key=key))
    right = key(max(array, key=key))

    holes = [[] for i in range(right - left + 1)]
    for value in array:
        holes[key(value) - left].append(value)

    array = []
    for pigeon in holes:
        array.extend(pigeon)

    return array[::-1] if reverse else array
