# docs:
#   http://hg.openjdk.java.net/jdk7/tl/jdk/file/bfd7abda8f79/src/share/classes/java/util/TimSort.java
#   http://svn.python.org/view/python/trunk/Objects/listobject.c?revision=69227&view=markup
#   http://svn.python.org/view/*checkout*/python/trunk/Objects/listsort.txt?revision=69846
#   http://www.hatfulofhollow.com/posts/code/timsort/index.html
#   http://en.wikipedia.org/wiki/Timsort
#   https://github.com/cortesi/sortvis/blob/master/libsortvis/algos/timsort.py


def sort(array, cmp=None, key=None, reversed=False):
    '''Hybrid sorting algorithm derived from mergesort and insertion sort.
    Used by Python and other stuff. Supposed to be awesome.
    TODO: gotta find a good description and/or pseudocode.
    '''
    if cmp is None:
        cmp = lambda x, y: x > y

    if key is None:
        key = lambda x: x

    return sorted(array, key=key, reverse=reversed)
