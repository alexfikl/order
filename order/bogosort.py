import random


def _sorted(array, key, reverse):
    n = len(array) - 1
    if reverse:
        return all([key(array[i]) >= key(array[i + 1]) for i in range(n)])
    else:
        return all([key(array[i]) <= key(array[i + 1]) for i in range(n)])


def sort(array, key=lambda x: x, reverse=False):
    '''A particularly ineffective type of sorting algorithm based on the
    generate and check paradigm, i.e randomly shuffle the array and check
    if it is sorted.

    Running time: O(n * n!) average with the possibility to never actually
    end.
    '''
    if len(array) <= 1:
        return array

    while not _sorted(array, key, reverse):
        random.shuffle(array)

    return array
