def sort(array, key=lambda x: x, reverse=False):
    '''Odd-Even Sort is a simple sorting algorithm based on Bubble Sort.

    It works by comparing all odd / even indexed adjacent pairs and swapping
    them if necessary.

    Running time: O(n ** 2).
    '''
    if len(array) <= 1:
        return array

    swapped = True
    while swapped:
        swapped = False
        for i in range(1, len(array), 2):
            if key(array[i - 1]) > key(array[i]):
                array[i], array[i - 1] = array[i - 1], array[i]
                swapped = True

        for i in range(2, len(array), 2):
            if key(array[i - 1]) > key(array[i]):
                array[i], array[i - 1] = array[i - 1], array[i]
                swapped = True

    return array[::-1] if reverse else array
