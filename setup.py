from distutils.core import setup, Command
from unittest import TextTestRunner, TestLoader
from glob import glob
import os

import order


class TestCommand(Command):
    description = "Custom test command that runs all tests in the \
                    `tests` directory."
    user_options = []

    def initialize_options(self):
        self.cwd = None

    def finalize_options(self):
        self.cwd = os.getcwd()

    def run(self):
        '''
        Finds all the tests modules in tests/, and runs them.
        '''
        testfiles = []
        for t in glob(os.path.join(self.cwd, 'tests', '*test.py')):
            testfiles.append('.'.join(
                ['tests', os.path.splitext(os.path.basename(t))[0]])
            )

        tests = TestLoader().loadTestsFromNames(testfiles)
        t = TextTestRunner(verbosity=3)
        t.run(tests)


setup(
    name=order.__title__,
    version=order.__version__,
    description='A collection of sorting algorithms',
    author=order.__author__,
    author_email='alexfikl@gmail.com',
    url='https://github.com/alexfikl/order',
    license=order.__license__,
    packages=['order'],
    platforms=["all"],
    cmdclass={'test': TestCommand}
)
