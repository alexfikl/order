import unittest
import string
import random as rand


def rstring(maxsize=10, chars=string.ascii_uppercase + string.ascii_lowercase):
    return "".join(rand.choice(chars) for x in range(rand.randrange(maxsize)))


class TestAlgorithms(unittest.TestCase):
    def test_unique(self):
        if self.sort is None:
            return

        # create our random sequences
        sortedseq = list(range(self.length))
        seq = sortedseq[:]
        rand.shuffle(seq)

        # and check
        self.assertEqual(self.sort(seq), sortedseq)

    def test_duplicate(self):
        if self.sort is None:
            return

        # created our random sequences with duplicates
        sortedseq = sorted(list(range(self.length)) + list(range(self.length)))
        seq = sortedseq[:]
        rand.shuffle(seq)

        # and check
        self.assertEqual(self.sort(seq), sortedseq)

    def test_reverse(self):
        if self.sort is None:
            return

        # create our random sequences
        sortedseq = list(reversed(range(self.length)))
        seq = sortedseq[:]
        rand.shuffle(seq)

        # and check
        self.assertEqual(self.sort(seq, reverse=True), sortedseq)

    def test_key(self):
        if self.sort is None:
            return

        # create our random sequences
        sortedseq = sorted([rstring() for x in range(self.length)], key=len)
        seq = sortedseq[:]
        rand.shuffle(seq)

        # get the actual keys from the values since not all sorts are stable
        seq = [len(x) for x in self.sort(seq, key=len)]
        sortedseq = [len(x) for x in sortedseq]

        # and check
        self.assertEqual(seq, sortedseq)


def addTests(algorithms):
    def get_setup(fnct, length):
        def setUp(self):
            self.length = length
            self.sort = fnct

        return setUp

    for alg, its in algorithms:
        fullmodule = __import__("order", fromlist=[alg])
        fnct = getattr(fullmodule, alg)

        testclass = type("Test{}".format(alg.capitalize()), (TestAlgorithms,),
                         {'setUp': get_setup(fnct, its)})

        globals()[testclass.__name__] = testclass


algorithms = [
    #('bogosort', 5),             # smaller because it could never end
    ('bubblesort', 32),
    ('insertionsort', 32),
    ('bitonicsort', 32),         # only works for powers of 2
    ('bucketsort', 32),
    ('cocktailsort', 32),
    ('combsort', 32),
    ('countingsort', 32),
    ('cyclesort', 32),
    ('gnomesort', 32),
    ('heapsort', 32),
    ('introsort', 32),
    ('mergesort', 32),
    #('librarysort', 32)
    ('oddevensort', 32),
    ('patiencesort', 32),
    ('pigeonholesort', 32),
    ('quicksort', 32),
    ('radixlsdsort', 32),       # only works on integers
    ('selectionsort', 32),
    ('shellsort', 32),
    ('stoogesort', 32)
]

addTests(algorithms)
del globals()["TestAlgorithms"]

if __name__ == "__main__":
    print(globals())
